<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFilterationsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_filterations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('filteration_name');
            $table->string('filteration_slug')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_filterations');
    }
}
