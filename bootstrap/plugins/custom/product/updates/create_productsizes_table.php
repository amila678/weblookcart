<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsizesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_productsizes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('product_size');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_productsizes');
    }
}
