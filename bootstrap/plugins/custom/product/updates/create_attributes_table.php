<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAttributesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_attributes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('attribute_name');
            $table->string('attribute_configuration')->nullable();
            $table->string('category_slug')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_attributes');
    }
}
