<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('Product_id');
            $table->text('Product_Name');
            $table->text('Product_color');
            $table->string('Product_size');
            $table->string('Product_Status');
            $table->string('Payment_Method');  
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_orders');
    }
}
