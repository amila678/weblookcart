<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAttributeconfsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_attributeconfs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('attribute_config');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_attributeconfs');
    }
}
