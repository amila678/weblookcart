<?php namespace Custom\Product;

use Backend;
use System\Classes\PluginBase;

/**
 * Product Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Product',
            'description' => 'Product Module for Weblook Cart',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Product\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'custom.product.some_permission' => [
                'tab' => 'Product',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'product' => [
                'label'       => 'Product',
                'url'         => Backend::url('custom/product/products'),
                'icon'        => ' icon-shopping-bag',
                'permissions' => ['custom.product.*'],
                'order'       => 500,
            ],
            
            'Order' => [
                'label'       => 'Order',
                'url'         => Backend::url('custom/product/orders'),
                'icon'        => ' icon-list-alt',
                'permissions' => ['custom.order.*'],
                'order'       => 600,
            ],
        ];
        
      
    }
    
    
    public function registerSettings()
	{
		return [
		  'attributes' => [
			  'label' => 'Attributes',
			  'description' => 'Manage Attributes',
			   'category' => 'Product Setting',
			   'icon' => 'icon-globe',
			   'url' => Backend::url('custom/product/attributes'),
			   
            ],
            
            'Categories' => [
                'label' => 'Categories',
                'description' => 'Managed Categories',
                'category' => 'Product Setting',
                'icon' => 'icon-sitemap',
                'url' => Backend::url('custom/product/Categories'),
                
            ],
            
            
            
            'Filterations' => [
                'label' => 'Filterations',
                'description' => 'Managed Filterations',
                'category' => 'Product Setting',
                'icon' => 'icon-filter',
                'url' => Backend::url('custom/product/Filterations'),
                
            ],
            
            'Status' => [
                'label' => 'Stockstatus',
                'description' => 'Managed Stock status',
                'category' => 'Product Setting',
                'icon' => 'icon-cubes',
                'url' => Backend::url('custom/product/Status'),
                
            ],
            
            
            'Color' => [
                'label' => 'colour',
                'description' => 'Managed colour',
                'category' => 'Product Setting',
                'icon' => 'icon-cubes',
                'url' => Backend::url('custom/product/Color'),
                
            ],
            
            
            'Productsizes' => [
                'label' => 'Size',
                'description' => 'Managed size setting on products',
                'category' => 'Product Setting',
                'icon' => 'icon-cubes',
                'url' => Backend::url('custom/product/Productsizes'),
                
            ],
            
            'trackstatuses' => [
                'label' => 'Trackstatuses',
                'description' => 'Managed tracking Status',
                'category' => 'Product Setting',
                'icon' => 'icon-cubes',
                'url' => Backend::url('custom/product/trackingstatus'),
                
            ]
            
            
            
            
            

        ];
        
	}
}
