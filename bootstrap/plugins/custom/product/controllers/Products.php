<?php namespace Custom\Product\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\product\Models\Product;
use Custom\product\controllers\Products;
use Db;


/**
 * Products Back-end Controller
 */
class Products extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Product', 'product', 'products');
    }
    
    
      /**
     * Start import from XML
     */
    public function onImportFromXML()
    {
        $obImport = new ImportProductModelFromXML();
        $obImport->import();

        $arReportData = [
            'created' => $obImport->getCreatedCount(),
            'updated' => $obImport->getUpdatedCount(),
            'skipped' => $obImport->getSkippedCount(),
            'processed' => $obImport->getProcessedCount(),
        ];

        Flash::info(Lang::get('lovata.toolbox::lang.message.import_from_xml_report', $arReportData));

        return $this->listRefresh();
    }

    /**
     * Start import from XML
     */
    public function onImportOffersFromXML()
    {
        $obImport = new ImportOfferModelFromXML();
        $obImport->import();

        $arReportData = [
            'created' => $obImport->getCreatedCount(),
            'updated' => $obImport->getUpdatedCount(),
            'skipped' => $obImport->getSkippedCount(),
            'processed' => $obImport->getProcessedCount(),
        ];

        Flash::info(Lang::get('lovata.toolbox::lang.message.import_from_xml_report', $arReportData));

        return $this->listRefresh();
    }

    /**
     * Start import from XML
     */
    public function onImportPricesFromXML()
    {
        $obImport = new ImportOfferPriceFromXML();
        $obImport->import();

        $arReportData = [
            'created' => $obImport->getCreatedCount(),
            'updated' => $obImport->getUpdatedCount(),
            'skipped' => $obImport->getSkippedCount(),
            'processed' => $obImport->getProcessedCount(),
        ];

        Flash::info(Lang::get('lovata.toolbox::lang.message.import_from_xml_report', $arReportData));

        return $this->listRefresh();
    }
    
    public function formAfterCreate($model)
    {
        $product_data                    =   Product::find($model->id);
        $product_id                       =   $product_data['product_id'];
        $productData                      =   Product::find($product_id);
        $product_data->save();
    }

    public function  productdetails()
    {
    
       $product_details = [];
       $products = Session::get('products');
       foreach ($products as $product){
          $data = [
          
            'ProductName' => $product['ProductName'],
            'Price'       => $product['Price'],
            'Sale Price'  => $product['Sale Price'],
            'Image'       => self::getImage($product['id']),
          
          
          ];
          
          array_push($product_details,$data);
          
        }
        
        return $product_details;
    }
    
}



// public static function getImage($id)
// {

// }