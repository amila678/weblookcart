<?php namespace Custom\Product\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Attributeconfs Back-end Controller
 */
class Attributeconfs extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Product', 'product', 'attributeconfs');
    }
}
