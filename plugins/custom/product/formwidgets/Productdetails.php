<?php namespace Custom\Product\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * Productdetails Form Widget
 */
class Productdetails extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'custom_product_productdetails';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('productdetails');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/productdetails.css', 'custom.product');
        $this->addJs('js/productdetails.js', 'custom.product');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
