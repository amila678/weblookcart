<?php namespace Custom\Product\Models;

use Model;
use Custom\product\Models\category;
use Custom\product\Models\attribute;
use Custom\product\Models\Stockstatuss;


/**
 * Product Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'custom_product_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    public function getCategoryOptions()
    {
        
        $Categories = Category::where('category_name', 0)->get()->toArray();
        $ret = [];
        foreach ($Categories as $Category) {
            $ret[$Category['category_name']] = $Category['category_name'];
        }
        return $ret;
		
    
    }
    
    public function getAttributeOptions()
    {
    
    
        $Attributes = attribute::where('attribute_name', 0)->get()->toArray();
        $ret = [];
        foreach ($Attributes as $Attribute) {
            $ret[$Attribute['id']] = $Attribute['attribute_name'];
        }
        return $ret;
      
    
    
    }
    
    
    public function getStockStatusOptions()
    {
    
        $Status = Status::where('stock_status', 0)->get()->toArray();
        $ret = [];
        foreach ($Status as $Status) {
            $ret[$Status['stock_status']] = $Status['stock_status'];
        }
        return $ret;
    
    }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
