<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTrackingstatusesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_trackingstatuses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('track_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_trackingstatuses');
    }
}
