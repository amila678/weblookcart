<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCurrenciesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_currencies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('currency');
            $table->string('currency_symbol');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_currencies');
    }
}
