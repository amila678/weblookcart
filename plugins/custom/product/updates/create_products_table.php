<?php namespace Custom\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_product_products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('ProductName')->nullable();
            $table->integer('Price')->nullable();
            $table->integer('Sale Price')->nullable();
            $table->longtext('Description');
            $table->string('Product_url');
            $table->string('Seo_Product');
            $table->string('Seo_Description');
            $table->string('Keywords')->nullable();
            $table->text('Attribute');
            $table->text('Category');
            $table->string('Image');
            $table->string('Gallery');
            $table->text('Inventory')->nullable();
            $table->text('stock_status');
            $table->string('Colour_selection')->nullable();
            $table->string('product_size')->nullable();
            $table->integer('Weight')->nullable();          
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_product_products');
    }
}
