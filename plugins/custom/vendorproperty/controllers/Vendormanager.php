<?php namespace Custom\Vendorproperty\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Vendormanager Back-end Controller
 */
class Vendormanager extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Vendorproperty', 'vendorproperty', 'vendormanager');
    }
}
