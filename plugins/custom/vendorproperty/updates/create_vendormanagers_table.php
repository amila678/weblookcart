<?php namespace Custom\Vendorproperty\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVendormanagersTable extends Migration
{
    public function up()
    {
        Schema::create('custom_vendorproperty_vendormanagers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_vendorproperty_vendormanagers');
    }
}
